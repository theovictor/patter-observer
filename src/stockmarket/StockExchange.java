package stockmarket;
import java.util.ArrayList;
public class StockExchange implements Subject{
    //List of Observers
    private ArrayList<Observer> obs;
    private double Dollar, Gold, Soy;
    public StockExchange() {
        //Instanciate a List to hold all observers
        obs = new ArrayList<Observer>();
    }
    @Override
    public void attach(Observer Obs) {
        //Adding to list
        if(obs!=null){
            obs.add(Obs);
        }
    }
    @Override
    public void detach(Observer Obs) {
        //Get the index of the observer to delete
        int obsIndex = obs.indexOf(Obs);
        //Increment index to match
        System.out.println("Observer "+(obsIndex+1)+" was removed!");
        //Removing observer from list
        obs.remove(obsIndex);
    }
    @Override
    public void notifyObservers() {
        /*Cycle throught all observers and notifies
        them of price changes*/
        for (Observer ob : obs) {
            ob.update(this.Dollar, this.Gold, this.Soy);
        }
    }
    public void setPrices(double Dollar, double Gold, double Soy){
        this.Dollar = Dollar;
        this.Gold = Gold;
        this.Soy = Soy;
        //Value change, notify Observers
        notifyObservers();
    }
    public double getDollarPrice(){
        return Dollar;
    }
    public double getGoldPrice(){
        return Gold;
    }
    public double getSoy() {
        return Soy;
    }
}