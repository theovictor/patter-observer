package stockmarket;
public class DollarStockHolder implements Observer{
    private double DollarPrice, ValueBuy, ValueSell;
    //Static variable used as a counter
    private static int obsIDTracker = 0;
    //Observer id
    private int obsID;
    //Will hold reference to the Market
    private Subject market;
    public DollarStockHolder(Subject market) {
        this.market = market;
        //Assign an Observer ID and increment static counter
        this.obsID = ++obsIDTracker;
        //Message of new Observer
        System.out.println("New Dollar Observer ID: "+this.obsID);
        //Adding observer to subject arraylist
        this.market.attach(this);
    }
    public void setValueBuy(double ValueBuy) {
        this.ValueBuy = ValueBuy;
    }
    public void setValueSell(double ValueSell) {
        this.ValueSell = ValueSell;
    }
    //Called to update all observers
    @Override
    public void update(double Dollar, double Gold, double Soy) {
        this.DollarPrice = Dollar;
        BuyAndSell();
    }
    private void BuyAndSell() {
        System.out.println("Dollar Observer ID: "+this.obsID+" Dollar Price: "+this.DollarPrice);
        if(this.DollarPrice > ValueSell){
            System.out.println("Selling Dollars!\n");
        }else if(this.DollarPrice <= ValueBuy){
            System.out.println("Buy Dollars!\n");
        }else{
            System.out.println("Holding Position!\n");
        }
    }
    
}
